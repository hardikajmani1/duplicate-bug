import pandas as pd
from tqdm import tqdm
from itertools import combinations
from matplotlib import pyplot as plt


df = pd.read_csv('../data/bug-dataset-mozilla.csv')

#gathering bug_id_dup_id and resolutions of bugs
bug_id = df['bug_id'].tolist()
dup_id = df['dup_id'].tolist()
resolution = df['resolution'].tolist()

no_dup_id = 0
no_bug_id = 0


for row in tqdm(range(df.shape[0])):
    record = df.iloc[row]
    bug_idx = record['bug_id']
    if record['resolution'] == 'DUPLICATE':
        dup_idx = record['dup_id']
        if not dup_idx:
            no_dup_id += 1
            #print("Empty Duplicate id {}".format(row[0]))
            df.at[row, 'resolution'] = 'NDUPLICATE'
        if int(dup_idx) not in bug_id:
            no_bug_id += 1
            #print("Duplicate id bug not present {}".format(row[0]))
            #row[1]['resolution'].replace('DUPLICATE', 'NDUPLICATE')
            df.at[row, 'resolution'] = 'NDUPLICATE'

list_of_non_dup_bugs = []

#find list of all bugs where dup_id is empty
for i in tqdm(range(df.shape[0])):
    details = df.iloc[i, :]
    bug_idx = details['bug_id']
    dup_idx = details['dup_id']
     
    if dup_idx == '[]':
        list_of_non_dup_bugs.append(bug_idx)

all_nd_pairs = combinations(list_of_non_dup_bugs, 2)

non_dup_df = pd.DataFrame(all_nd_pairs, columns=['bug1', 'bug2'])

non_dup_df.to_csv('./non_duplicate_bug_pairs.csv' ,index=False)